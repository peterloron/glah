#!groovy
pipeline {
  agent any

  options {
    disableConcurrentBuilds()
    skipDefaultCheckout()
  }
  environment {
    build_status = "SUCCESS"
  }
  
  stages{
    stage ("Environment Setup and Configuration") {
      steps {
        checkout([
          $class: 'GitSCM',
          branches: [[name: 'refs/heads/master']],
          userRemoteConfigs: [[
            url: 'https://gitlab.com/peterloron/glah.git'
          ]]
        ])
        script {
          config = readYaml (file: 'images.yaml')
        }
      }
    }

    stage("Mirror images") {
      steps {
        script {

          config.images.each{ key, value ->
            //pull the image
            source = docker.image(key)
            try {
              retry(10) {
                source.pull()
              }
            } catch(Exception ex) {
              print("Failed to pull source image!")
              build_status = 'FAILURE'
              return
            }

            // push to destinations
            config.destinations.each{ dkey, dvalue ->
              print("Image: ${key}, URL: ${dkey.url}, PassID: ${dkey.pass}")
              try {
                withDockerRegistry(credentialsId: "${dkey.pass}", url: "https://${dkey.url}") {
                  sh "docker tag ${key} ${dkey.url}/${key}"
                  target = docker.image("${dkey.url}/${key}")
                  retry(10) {
                    target.push()
                  }
                }
              } catch(Exception ex) {
                print("Failed to push image!")
                build_status = 'FAILURE'
              }
            }
          }
        }
      }
    }
  }

  post {
    always {
      script {
        currentBuild.result = build_status
      }
    }
  }
}